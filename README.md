# ArchiViz

## Installation du projet

- Installer GitHub Desktop pour plus de facilité au niveau du versioning : https://desktop.github.com/

<p align="center">
  <img src="https://user-images.githubusercontent.com/93186642/213466880-803d8dc1-e0c7-4969-8c9c-cb81e26e605e.png" width="700"/>
</p>

- Cloner le Repo ArchiViz dans un dossier local à votre ordinateur

- Aller dans le dossier où vous avez cloné le Repo

- Faire un clic droit sur la solution du projet "ArchiViz.uproject"

- Cliquer sur l'option "Generate visual studio project files" dans le menu déroulant

<p align="center">
  <img src="https://user-images.githubusercontent.com/93186642/213465380-a2084cb2-0642-4c50-9b46-1d316dd4d4bd.png" width="700"/>
</p>

Après cette phase de génération, pour lancer le projet sous Unreal Engine :

- Double cliquer sur la solution du projet "ArchiViz.uproject"

- Une fenêtre d'erreur va s'ouvrir vous proposant de re-build "ArchiViz", cliquez sur le bouton "oui"

- Suite à ces étapes, le projet va enfin se lancer sans problème sur votre PC
