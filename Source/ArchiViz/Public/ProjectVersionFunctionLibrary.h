// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProjectVersionFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class ARCHIVIZ_API UProjectVersionFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Get Project Version"), Category = "System Information")
		static FString GetProjectVersion();
};
