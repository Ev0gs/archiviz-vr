// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectVersionFunctionLibrary.h"

FString UProjectVersionFunctionLibrary::GetProjectVersion() 
{
	FString GetProjectVersion;
	GConfig->GetString(
		TEXT("/Script/engineSettings.GeneralProjectSettings"),
		TEXT("ProjectVersion"),
		GetProjectVersion,
		GGameIni
	);
	return GetProjectVersion;
}